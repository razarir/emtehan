<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tosifi extends Model
{
 protected $appends = ['a_very_good','a_acceptable','a_need_to_try','a_well'];
 protected $hidden = ['need_to_try', 'well', 'acceptable', 'very_good','created_at','updated_at','id'];


 public function getAVeryGoodAttribute()
 {
  return $this->getArray_diff($this->very_good);

 }
 public function getAWellAttribute()
 {
  return $this->getArray_diff($this->well);

 }
 public function getAAcceptableAttribute()
 {
  return $this->getArray_diff($this->acceptable);

 }
 public function getANeedToTryAttribute()
 {
  return $this->getArray_diff($this->need_to_try);

 }

 /**
  * @return false|string[]
  */
 public function getArray_diff($s)
 {
  $reandom_sentence = $this->reandom_sentence($s);
//  dump($reandom_sentence);
  return

   $reandom_sentence;
//  dump($this->check_min_count($s));
//  dump(array_unique($this->check_min_count($s)));
//  return array_unique($this->check_min_count($s));
//  for ($x = 0; $x <=  1 ; $x++) {
//  }
 }

 /**
  * @param int $num
  * @return int
  */
 public function factorial(int $num): int
 {
  $factorial = 1;
  for ($x = $num; $x >= 1; $x--) {
   $factorial = $factorial * $x;
  }
  return $x;
 }

 /**
  * @param $s

  */
 public function reandom_sentence($s)
 {
//  dump('reandom_sentence');
  list($list, $words) = $this->splitor($s);
  $export = [];
  foreach ($list as $key=>$value) {
   if (count($value)<=3) {
//    dump('continue;');
//    continue;
//    break;
    $export[$key] = [];
//    for ($x = 0; $x <= 0; $x++) {


     $implitor = $this->implitor($value);
//   dump($implitor);
//   dump('$implitor1->'.$key,$implitor,'<-2$implitor'.$key);

     array_push($export[$key], $implitor);
//   return $implitor;
//    }
//    $export[$key] = array_unique($export[$key]);


   }else{
    $export[$key] = [];
    for ($x = 0; $x <= 10; $x++) {


     $implitor = $this->implitor($value);
//   dump($implitor);
//   dump('$implitor1->'.$key,$implitor,'<-2$implitor'.$key);

     array_push($export[$key], $implitor);
//   return $implitor;
    }
    $export[$key] = array_unique($export[$key]);
   }



  }
//  dump($export);
  return $export;
 }

 /**
  * @param $s
  * @return array[]
  */
 public function splitor($s): array
 {
  $array_diff = array_diff(explode('.', $s), ['']);
//  dump($array_diff);
  $list = [];
  foreach ($array_diff as $key => $value1) {
   $words = [];
   $explode = explode(' و ', $value1);
   foreach ($explode as $value2) {
    foreach (explode(' ، ', $value2) as $value3) {
     array_push($words, $value3);
    }
   }

   array_push($list, $words);
   $words = [];

  }
  return array($list, $words);
//  dump($list);
 }

 /**
  * @param array $list
  * @return string
  */
 public function implitor(array $list): string
 {
  $list_between = $list;
  array_shift($list_between);
  array_splice($list_between, count($list_between) - 1);
  $random_sentence = '';
  shuffle($list_between);


  $and = [' ، ', ' و '];

  $max = count($list_between) - 1;
  for ($x = 0; $x <= rand(0, $max); $x++) {
   if ($x === 0) {
    $random_sentence .= $and[rand(0, 1)];
   }
   if (isset($list_between[$x])) {
    $random_sentence .= $list_between[$x];
   }
   else {
    continue;
   }
   array_splice($list_between, $x);
   $random_sentence .= $and[rand(0, 1)];
  }

  $sentence = $list[0] . $random_sentence . $list[count($list) - 1];
//  dump($sentence);
//  return $array_diff;
  return $sentence;
 }

 /**
  * @param $s
  * @return array
  */
 public function check_min_count($s,$deep=5): array
 {
//  dump('check_min_count');
  $export = [];
  $reandom_sentence = $this->reandom_sentence($s);
//  $count = count($reandom_sentence);
 $export= array_merge($export, $reandom_sentence);
//
  if (count($export) < 5 && $deep>0) {
//   dump('')
   $deep = $deep-1;
   $export=array_merge($export, $this->check_min_count($s,$deep));
  }
//  dump('$reandom_sentence>',$reandom_sentence,'<w$reandom_sentence');
  dump('$reandom_sentence>',$export,'<w$reandom_sentence');
//  dump($export);
  return $export;
 }


}