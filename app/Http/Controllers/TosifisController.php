<?php

namespace App\Http\Controllers;

use App\Models\Tosifi;
use App\Models\User;
use Illuminate\Http\Request;

class TosifisController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  return Tosifi::all();
 }

 /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function create()
 {
  //
 }

 /**
  * Store a newly created resource in storage.
  *
  * @param Request $request
  * @return \Illuminate\Http\Response
  */
 public function store(Request $request)
 {
  Tosifi::whereLessonName('قرآن')->first();
 }

 /**
  * Display the specified resource.
  *
  * @param Tosifi $tosifi
  * @return Tosifi
  */
 public function show($id)
 {


  $lesson = Tosifi::whereLessonName($id)->first();


  $users = User::select('name','last_name','score','code')->get();
 $users= collect($users)->map(function ($user) use($lesson){
  $collection = collect($user);

  $score = $collection['score'];
  $score = $this->score($score);
  $collection['score']=$score;

  return $collection
    ->put('text', $this->text($lesson,$score));
  });

  $countBy = $users->countBy('score');
  if ($countBy['1']+$countBy['2']<$countBy['3']) {
   $this->show($id);
  }else{
   $title = $id;
   return view('list', compact('users','title'));
  return [$users,$countBy,$users->groupBy('score')];
  }


 }

 /**
  * Show the form for editing the specified resource.
  *
  * @param Tosifi $tosifi
  * @return \Illuminate\Http\Response
  */
 public function edit(Tosifi $tosifi)
 {
  //
 }

 /**
  * Update the specified resource in storage.
  *
  * @param Request $request
  * @param Tosifi $tosifi
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, Tosifi $tosifi)
 {
  //
 }

 /**
  * Remove the specified resource from storage.
  *
  * @param Tosifi $tosifi
  * @return \Illuminate\Http\Response
  */
 public function destroy(Tosifi $tosifi)
 {
  //
 }


 public function randArray($array, $count = 1)
 {
//  dump('randArray');
//  $txt = '';

  shuffle($array);
  $count1 = count($array);
  if ($count1 < $count) {
   $count = $count1;
  }
   array_splice($array, 0, $count);
  $list = $array;
//  dump($list,'list');
  $export = [];
  foreach ($list as $value) {
   shuffle($value);
//   $txt .= $value[0] . '.';
//   dump($export, $value[0],'////');
    array_push($export, $value[0]);
  }
//  $list = [];
//  for ($x = 1; $x <= $count; $x++) {
//
//   $count1 = count($array);
//   if ($count1>=1 ) {
//    list($array, $list) = $this->checkUntilRandomIsInArray($count1, $array, $list);
//   }
//
////   dump($array_rand,'**',$list,'**',$array[$array_rand],'**', $array);
//  }
////  return $list;
////  dump($list);
//
//  return implode('.', $list[rand(0,count($list)-1)]);
//  return $txt;
//  dump('export', $export);
  return $export;
 }

 /**
  * @param int $count1
  * @param $array
  * @param array $list
  * @return array
  */
 public function checkUntilRandomIsInArray(int $count1, $array, array $list): array
 {
  $array_rand = rand(0, $count1 - 1);
  if (isset($array[$array_rand])) {
   array_push($list, $array[$array_rand]);
   array_splice($array, $array_rand);
  }
  else {
   $this->checkUntilRandomIsInArray($count1, $array, $list);
  }
  return array($array, $list);
 }



 public function text($lesson,$score)
 {
  if ($score === 1) {
   [$sub,$main]= $this->main_sub();
   $list = $this->randArray($lesson->a_very_good, $main);
   if ($list === []) {
   return $this->text($lesson,$score);
   }else {
    return implode('.', $list);
   }
  }
  elseif ($score === 2) {
   [$sub,$main]= $this->main_sub();
   $list = $this->randArray($lesson->a_well, $main);
   if ($list === []) {
    return $this->text($lesson,$score);
   }else {
    return implode('.', $list);
   }
  }
  elseif ($score === 3) {

   [$sub,$main]= $this->main_sub();
  $list = $this->randArray($lesson->a_acceptable, $main);
   if ($list === []) {
   return $this->text($lesson,$score);
   }else {
    return implode('.', $list);
   }

   }

//  }
//  elseif ($score === 4) {
//   return $this->randArray($lesson->a_need_to_try, 2);
//
//  }

 }

 /**
  * @param $score
  * @return int
  */
 public function score($score): int
 {
  if ($score === 1) {
   $score = rand(1, 2);
  }
  elseif ($score === 2) {
   $score = rand(1, 3);
  }
  elseif ($score === 3) {
   $score = rand(2, 3);
  }
  else {
   $score = 2;
  }
  return $score;
 }

 public function main_sub()
 {
  $all = rand(1, 4);
  $main = rand(1, 3);
  if ($all >= $main) {
   $sub = $all - $main;
//   dump($sub, $main);
   if ($sub > $main) {
    $this->main_sub();
   }else{
    return [$sub,$main];
   }
  }else{
   $this->main_sub();
  }

 }
}

