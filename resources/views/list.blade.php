<!doctype html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <meta name="viewport"
       content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
 <meta http-equiv="X-UA-Compatible" content="ie=edge">
 <title>{{$title}}</title>
 <!-- Latest compiled and minified CSS -->
 <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
 <script>
   function t($id) {
     // alert($id)
     /* Get the text field */
     var copyText = document.getElementById($id);

     // alert(copyText);
     console.log(copyText.value)
     /* Select the text field */
     copyText.select();
     copyText.setSelectionRange(0, 99999); /* For mobile devices */

     /* Copy the text inside the text field */
     document.execCommand("copy");

     /* Alert the copied text */
     // alert("Copied the text: " + copyText.value);
   }
 </script>
</head>
<body class="rtl" dir="rtl">

<div class="container">
 <table class="table table-bordered table-hover ">
  <th>
   <tr>
    <td>نام</td>
    <td>نمره</td>
    <td>متن</td>
   </tr>
  </th>
  @foreach($users as $key=>$user)
  <tr>
   <td>{{$user['name']}}</td>
   <td onclick="t({{$user['code']}})" >
    <input style="width: 100px" type="text"  id="{{$user['code']}}" value="{{$user['score']}}">
   </td>
   <td onclick="t({{$key}})" >

    <textarea name="" id="{{$key}}" cols="180" rows="2">
     {{$user['text']}}
    </textarea>
   </td>
  </tr>

  @endforeach
 </table>
</div>

</body>
</html>