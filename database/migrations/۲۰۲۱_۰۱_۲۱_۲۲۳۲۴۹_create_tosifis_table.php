<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTosifisTable extends Migration
{
 public function up()
 {
  Schema::create('tosifis', function (Blueprint $table) {
   $table->bigIncrements('id');

   $table->longText('lesson_name');
   $table->longText('very_good');
   $table->longText('well');
   $table->longText('acceptable');
   $table->longText('need_to_try');
   $table->timestamps();
  });
 }

 public function down()
 {
  Schema::dropIfExists('tosifis');
 }
}