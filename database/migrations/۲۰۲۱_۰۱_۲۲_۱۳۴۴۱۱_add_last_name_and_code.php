<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLastNameAndCode extends Migration
{
 public function up()
 {
  Schema::table('users', function (Blueprint $table) {
   $table->string('last_name')->after('name');
   $table->string('code')->after('last_name');
  });
 }

 public function down()
 {
  Schema::table('users', function (Blueprint $table) {
   $table->dropColumn('last_name');
   $table->dropColumn('code');
  });
 }
}